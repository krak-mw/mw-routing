<?php

namespace Krak\Mw\Routing;

use FastRoute,
    Psr\Http\Message\ServerRequestInterface;

require_once __DIR__ . '/convert-params.php';
require_once __DIR__ . '/invoke-action.php';
require_once __DIR__ . '/marshal-response.php';

function fastRouteRouter(FastRoute\Dispatcher $dispatcher) {
    return function(ServerRequestInterface $req) use ($dispatcher) {
        $method = $req->getMethod();
        $uri = $req->getUri()->getPath();

        $res = $dispatcher->dispatch($method, $uri);
        if ($res[0] == FastRoute\Dispatcher::FOUND) {
            return [200, $res[1], $res[2]];
        }
        if ($res[0] == FastRoute\Dispatcher::NOT_FOUND) {
            return [404];
        }
        if ($res[0] == FastRoute\Dispatcher::METHOD_NOT_ALLOWED) {
            return [405, $res[1]];
        }
    };
}

function routingMw($router, $invoke_action, $handle_error) {
    return function(ServerRequestInterface $req, $next) use ($router, $invoke_action, $handle_error) {
        $res = $router($req);

        if ($res[0] != 200) {
            return $handle_error($req, $res);
        }

        return $invoke_action($req, $res[1], $res[2]);
    };
}

/** instead of returning a response, it simply just injects the routing parameters
    into the route */
function routingInjectMw($router, $handle_error, $prefix = '') {
    return function(ServerRequestInterface $req, $next) use ($router, $handle_error, $prefix) {
        $res = $router($req);

        if ($res[0] != 200) {
            return $handle_error($req, $res);
        }

        return $next(
            $req->withAttribute($prefix . 'action', $res[1])
                ->withAttribute($prefix . 'params', $res[2])
        );
    };
}

/** simply invoke the action from the attributes set earlier */
function invokeActionMw($invoke_action, $prefix = '') {
    return function(ServerRequestInterface $req, $next) use ($invoke_action, $prefix) {
        return $invoke_action(
            $req,
            $req->getAttribute($prefix . 'action'),
            $req->getAttribute($prefix . 'params')
        );
    };
}
