<?php

namespace Krak\Mw\Routing;

use Psr\Http\Message\ServerRequestInterface;

/** this takes a controller result and turns it into a response */
interface MarshalResponse {
    public function __invoke($result, ServerRequestInterface $req);
}

/** Takes a 2-tuple of the 302 status code and the uri to redirect to
    This is a decorator and sits on top of another response marshaler. If the
    result doesn't match, it will delegate to the inner marshaler */
function redirectMarshalResponse($marshal, $rf) {
    return function($result, ServerRequestInterface $req) use ($marshal, $rf) {
        if (!is_array($result) || count($result) != 2) {
            return $marshal($result, $req);
        }

        list($code, $path) = $result;
        if ($code != 302 || !is_string($path)) {
            return $marshal($result, $req);
        }

        return $rf(302, ['Location' => $path]);
    };
}
