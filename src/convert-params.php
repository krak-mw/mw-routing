<?php

namespace Krak\Mw\Routing;

use Psr\Http\Message\ServerRequestInterface;

interface ConvertParams {
    public function __invoke($params, ServerRequestInterface $req);
}
