<?php

use Krak\Mw\Routing,
    GuzzleHttp\Psr7;

describe('Mw Routing', function() {
    describe('InvokeAction', function() {
        beforeEach(function() {
            $this->req = new Psr7\ServerRequest('GET', '/');
        });
        describe('#callableInvokeAction', function() {
            it('invokes the action as a callable', function() {
                $invoke = Routing\callableInvokeAction();
                $action = function($req) {
                    return $req;
                };

                assert($this->req === $invoke($this->req, $action, []));
            });
            it('invokes the action and passes the params to a callable', function() {
                $invoke = Routing\callableInvokeAction(true);
                $action = function($req, $params) {
                    return $params;
                };

                assert('abc' == $invoke($this->req, $action, 'abc'));
            });
            it('throws exception if action is not a callable', function() {
                $invoke = Routing\callableInvokeAction();

                try {
                    $invoke($this->req, null, []);
                    assert(false);
                } catch (InvalidArgumentException $e) {
                    assert(true);
                }
            });
        });
        describe('#pimpleInvokeAction', function() {
            beforeEach(function() {
                $this->container = new Pimple\Container();
                $this->container['namespace.prefix.controller'] = function() {
                    return new StdClass();
                };
                $this->container['namespace.prefix.action'] = function() {
                    return function($req) {
                        return $req;
                    };
                };
                $this->invoke = function($req, $action, $params) { return $action; };
            });
            it('delegates to invoker if parameter is not in service', function() {
                $invoke = Routing\pimpleInvokeAction($this->invoke, $this->container, 'namespace.prefix.');
                assert('asis' == $invoke($this->req, 'asis', []));
            });
            it('grabs service from container before delegating', function() {
                $invoke = Routing\pimpleInvokeAction($this->invoke, $this->container, 'namespace.prefix.');
                assert($invoke($this->req, 'action', []) instanceof Closure);
            });
            it('grabs service from container and method before delegating', function() {
                $invoke = Routing\pimpleInvokeAction($this->invoke, $this->container, 'namespace.prefix.');
                assert($invoke($this->req, 'controller@method', [])[1] == 'method');
            });
        });
    });
    describe('Marshal Response', function() {
        require __DIR__ . '/marshal-response.php';
    });
});
