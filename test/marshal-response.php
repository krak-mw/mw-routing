<?php

use Krak\Mw\Routing;

describe('#redirectMarshalResponse', function() {
    beforeEach(function() {
        $this->req = new GuzzleHttp\Psr7\ServerRequest('GET', '/');
        $this->rf = function() {
            return func_get_args();
        };
    });

    it('passes along to inner marshal if not valid tuple', function() {
        $marshal = Routing\redirectMarshalResponse(function($res, $req) {
            return 'marshalled';
        }, $this->rf);

        $res = $marshal('abc', $this->req);
        assert($res == 'marshalled');
    });
    it('passes along to inner marshal if tuple parts are not valid', function() {
        $marshal = Routing\redirectMarshalResponse(function($res, $req) {
            return 'marshalled';
        }, $this->rf);
        $res = $marshal([302, 323], $this->req);
        assert($res == 'marshalled');
    });
    it('returns a 302 response if tuple is correct', function() {
        $marshal = Routing\redirectMarshalResponse(function(){}, $this->rf);
        $res = $marshal([302, ''], $this->req);
        assert($res[0] == 302 && is_array($res[1]));
    });
});
